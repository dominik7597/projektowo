<?php

namespace Tests\Feature;

use App\Models\Project;
use App\Models\Task;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ActivitiesTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    use RefreshDatabase;

    public function test_user_create_project_activity()
    {
        $this->signIn();
        $project = Project::factory()->create();
        $this->assertCount(1, $project->activities);
    }

    public function test_user_update_project_activity()
    {
        $this->signIn();
        $project = Project::factory()->create();
        $project->update(['title' => 'New title']);
        $this->assertCount(2, $project->activities);
    }

    public function test_activity_needs_description()
    {
        $this->signIn();
        $project = Project::factory()->create();
        $this->assertEquals('create_project', $project->activities[0]->description);
    }

    public function test_user_add_project_task_activity()
    {
        $this->signIn();
        $project = Project::factory()->create(['user_id' => auth()->id()]);
        $project->createTask('Description');
        $this->assertCount(2, $project->allActivities()->get());
        $this->assertInstanceOf(Task::class, $project->allActivities()->get()->last()->subject);
    }

    public function test_task_achieved_activity()
    {
        $this->signIn();
        $project = Project::factory()->create(['user_id' => auth()->id()]);
        $task = $project->createTask('Description');

        $this->patch($task->path(), [
            'description' => 'new description',
            'achieved' => date('Y-m-d H:i:s')
        ]);

        $this->patch($task->path(), [
            'description' => 'new description',
            'achieved' => null
        ]);

        $this->assertCount(4, $project->allActivities()->get());
    }

    public function test_task_deleted_activity()
    {
        $this->signIn();
        $project = Project::factory()->create(['user_id' => auth()->id()]);
        $project->createTask('Description');

        $project->tasks[0]->delete();

        $this->assertCount(2, $project->allActivities()->get());
    }

    public function test_request_created_activity()
    {
        $this->withoutExceptionHandling();
        $this->signIn();
        $user = User::factory()->create();
        $project = Project::factory()->create();
        $this->post('/projects/'.$project->id.'/request/'.$user->id);
        $this->assertDatabaseHas('activities', [
            'description' => 'create_request'
        ]);
    }
}
