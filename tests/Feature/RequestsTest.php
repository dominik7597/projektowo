<?php

namespace Tests\Feature;

use App\Models\Project;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class RequestsTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    public function test_user_can_create_join_requests()
    {
        $this->withoutExceptionHandling();
        $this->signIn();
        $user = User::factory()->create();
        $project = Project::factory()->create();
        $project2 = Project::factory()->create();
        $this->post('/projects/'.$project->id.'/request/'.$user->id);
        $this->post('/projects/'.$project2->id.'/request/'.$user->id);
        //dd($user->request->pluck('pivot.status'));
//        $user->request->first()->pivot->status = 'accepted';
//        $user->request->first()->pivot->save();
        //dd($user->request->pluck('user_id'));
        $this->assertDatabaseHas('project_request', [
            'status' => 'pending'
        ]);

    }
}
