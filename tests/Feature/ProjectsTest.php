<?php

namespace Tests\Feature;

use App\Models\Project;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ProjectsTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    public function test_user_create_project()
    {
        $this->signIn();

        $this->get('/projects/create')->assertStatus(200);

        $attributes = [
            'title' => $this->faker->sentence,
            'short_body' => $this->faker->sentence,
            'body' => $this->faker->sentence,
        ];

        $response = $this->post('/projects', $attributes);

        $project = Project::where($attributes)->first();

        $response->assertRedirect($project->path());

        $this->assertDatabaseHas('projects', $attributes);
        $this->get($project->path())->assertSee($attributes['title'])
            ->assertSee($attributes['short_body'])
            ->assertSee($attributes['body']);
    }

    public function test_user_update_project()
    {
        $this->withoutExceptionHandling();

        $this->signIn();

        $project = Project::factory()->create(['user_id' => auth()->id()]);

        $this->patch($project->path(), [
            'title' => 'New title',
            'short_body' => 'New short body',
            'body' => 'New body'
        ]);

        $this->assertDatabaseHas('projects', [
            'title' => 'New title',
            'short_body' => 'New short body',
            'body' => 'New body'
        ]);
    }

    public function test_user_delete_project()
    {
        $this->signIn();

        $project = Project::factory()->create(['user_id' => auth()->id()]);
        $project->finish();

        $this->delete($project->path());

        $this->assertNull($project->fresh());
    }

    public function test_project_needs_title()
    {
        $this->signIn();

        //nadpisuje tytuł na pusty, raw zwraca tablice
        $attributes = Project::factory()->raw(['title' => '']);
        $this->post('/projects', $attributes)->assertSessionHasErrors(['title']);
    }

    public function test_project_needs_body()
    {
        $this->signIn();
        $attributes = Project::factory()->raw(['body' => '']);
        $this->post('/projects', $attributes)->assertSessionHasErrors(['body']);
    }

    public function test_user_view_own_project()
    {
        $this->signIn();
        $project = Project::factory()->create(['user_id' => auth()->id()]);
        $this->get($project->path())->assertSee($project->title)->assertSee($project->body);
    }

    public function test_user_needs_authentication_to_create_project()
    {
        $this->get('/projects/create')->assertRedirect('login');
        $attributes = Project::factory()->raw(['user_id' => null]);
        $this->post('/projects', $attributes)->assertRedirect('login');
    }

    public function test_user_needs_authentication_to_edit_projects()
    {
        $this->signIn();
        $project = Project::factory()->create();
        $this->signOut();
        $this->get($project->path() . '/edit')->assertRedirect('login');
    }

    public function test_user_needs_authentication_to_view_projects()
    {
        $this->get('/projects')->assertRedirect('login');
    }

    public function test_user_needs_authentication_to_view_project()
    {
        $this->signIn();
        $project = Project::factory()->create();
        $this->signOut();
        $this->get($project->path())->assertRedirect('login');
    }

    public function test_user_needs_authentication_to_delete_project()
    {
        $this->signIn();
        $project = Project::factory()->create();

        $this->delete($project->path())->assertStatus(403);

        $this->signOut();
        $this->delete($project->path())->assertRedirect('login');

        $user = User::factory()->create();
        $project->invite($user);
        $this->actingAs($user)->delete($project->path())->assertStatus(403);
    }

    public function test_user_cant_see_projects_by_others()
    {
        $this->signIn();

        $project = Project::factory()->create();
        $this->get($project->path())->assertStatus(403);
    }

    public function test_user_cant_update_projects_by_others()
    {
        $this->signIn();
        $project = Project::factory()->create();
        $this->patch($project->path())->assertStatus(403);
    }

    public function test_team_see_project()
    {
        $this->signIn($user = User::factory()->create());

        $project = Project::factory()->create();
        $project->invite($user);

        $this->get('/projects')->assertSee($project->title);
    }

    public function test_project_can_be_finished()
    {
        $this->signIn();
        $project = Project::factory()->create(['user_id' => auth()->id()]);
        $this->patch($project->path(), ['finished' => date('Y-m-d H:i:s')]);
        $this->assertDatabaseHas('projects', [
            'finished' => date('Y-m-d H:i:s')
        ]);
    }
}
