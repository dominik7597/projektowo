<?php

namespace Tests\Feature;

use App\Models\Project;
use App\Models\Task;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class TasksTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    use RefreshDatabase;

    public function test_task_need_description()
    {
        $this->signIn();

        $project = Project::factory()->create(['user_id' => auth()->id()]);

        //nadpisuje tytuł na pusty, raw zwraca tablice
        $attributes = Task::factory()->raw(['description' => '']);
        $this->post( $project->path() . '/tasks', $attributes)->assertSessionHasErrors(['description']);
    }

    public function test_project_can_have_tasks()
    {
        $this->signIn();

        $project = Project::factory()->create(['user_id' => auth()->id()]);

        $this->post($project->path() . '/tasks', ['description' => 'Test description']);
        $this->get($project->path())->assertSee('Test description');
    }

    public function test_user_needs_authentication_to_create_task()
    {
        $this->signIn();
        $project = Project::factory()->create();
        $this->signOut();
        $this->post($project->path() . '/tasks')->assertRedirect('login');
    }

    public function test_user_cant_create_or_update_tasks_to_projects_by_others()
    {
        $this->signIn();
        $project = Project::factory()->create();

        $this->post($project->path() . '/tasks', ['description' => 'Test description'])->assertStatus(403);
        $this->assertDatabaseMissing('tasks', ['description' => 'Test description']);

        $project = Project::factory()->create();
        $task = $project->createTask('Test Description');

        $this->patch($task->path(), ['description' => 'New description'])->assertStatus(403);
        $this->assertDatabaseMissing('tasks', ['description' => 'New description']);
    }

    public function test_task_update()
    {
        $this->withoutExceptionHandling();

        $this->signIn();

        $project = Project::factory()->create(['user_id' => auth()->id()]);

        $task = $project->createTask('Test description');

        $this->patch($task->path(), [
            'description' => 'new description',
            'achieved' => date('Y-m-d H:i:s')
        ]);

        $this->assertDatabaseHas('tasks', [
            'description' => 'new description',
            'achieved' => date('Y-m-d H:i:s')
        ]);
    }
}
