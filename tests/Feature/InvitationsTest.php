<?php

namespace Tests\Feature;

use App\Models\Project;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class InvitationsTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    use RefreshDatabase;

    public function test_invite_others_to_project()
    {
        $this->withoutExceptionHandling();

        $this->signIn();
        $project = Project::factory()->create();
        $userToInvite = User::factory()->create();

        $this->actingAs($project->user)->post($project->path() . '/invitations', [
            'email' => $userToInvite->email
        ]);

        $this->assertTrue($project->fresh()->team->contains($userToInvite));
    }

    public function test_user_must_not_already_be_invited()
    {
        $this->signIn();
        $project = Project::factory()->create();
        $invitedUser = User::factory()->create();

        $project->invite($invitedUser);

        $this->actingAs($project->user)->post($project->path() . '/invitations', [
            'email' => $invitedUser->email
        ])->assertSessionHasErrors([
            'email' => 'Ten użytkownik jest już członkiem projektu.'
        ]);
    }


    public function test_user_cant_invite_to_projects_by_others()
    {
        $this->signIn();
        $project = Project::factory()->create();
        $user = User::factory()->create();
        $this->actingAs($user)->post($project->path() . '/invitations')->assertStatus(403);
    }

    public function test_invited_user_need_account()
    {
        $this->signIn();
        $project = Project::factory()->create();

        $this->actingAs($project->user)->post($project->path() . '/invitations', [
            'email' => 'noemail@test'
        ])->assertSessionHasErrors(['email']);
    }

    public function test_team_can_add_tasks()
    {
        $this->signIn();
        $project = Project::factory()->create(['user_id' => auth()->id()]);
        $project->invite($user = User::factory()->create());
        $this->patch($project->path() . '/team/' . $user->id, ['role' => 'update']);
        $this->signIn($user);
        $this->post($project->path() . '/tasks', ['description' => 'Test description']);
        $this->assertDatabaseHas('tasks', ['description' => 'Test description']);
    }
}
