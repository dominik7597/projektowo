<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class UsersTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    use RefreshDatabase;

    public function test_user_need_authentication_to_view_profile()
    {
            $user = User::factory()->create();
            $this->get($user->path())->assertRedirect('login');
    }

    public function test_user_update_profile()
    {
        $this->withoutExceptionHandling();

        $user = User::factory()->create();
        $this->signIn($user);

        $newPassword = 'newpassword';
        $this->patch($user->path(), [
            'name' => 'newName',
            'email' => 'email@new',
        ]);

        $this->assertDatabaseHas('users', [
            'name' => 'newName',
            'email' => 'email@new',
        ]);

        $this->patch($user->path(), [
            'name' => 'newName',
            'email' => 'email@new',
            'password' => $newPassword
        ]);

        $this->assertTrue(Hash::check($newPassword,$user->fresh()->password));
    }
}
