<?php

namespace Tests\Feature;

use App\Models\Project;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class RolesTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    use RefreshDatabase;

    public function test_team_member_has_role()
    {
        $this->withoutExceptionHandling();
        $this->signIn();
        $project = Project::factory()->create();
        $user = User::factory()->create();
        $project->invite($user);
        $this->assertEquals($project->fresh()->team->skip(1)->get(1)->pivot->role, 'view');
    }

    public function test_team_member_role_can_be_changed()
    {
        $this->withoutExceptionHandling();
        $this->signIn();
        $project = Project::factory()->create(['user_id' => auth()->id()]);
        $user = User::factory()->create();
        $project->invite($user);
        $this->patch($project->path() . '/team/' . $user->id, ['role' => 'update']);
        $this->assertEquals($project->fresh()->getTeamMemberRole($user), 'update');
    }

    public function test_viewRole_cant_update_projects()
    {
        $this->signIn();
        $project = Project::factory()->create();
        $user = User::factory()->create();
        $project->invite($user);
        $this->actingAs($user)->post($project->path() . '/tasks', ['description' => 'Test description'])->assertStatus(403);
    }
}
