<?php

namespace Tests\Unit;

use App\Models\Project;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ActivityTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    use RefreshDatabase;

    public function test_activity_needs_user()
    {
        $this->signIn();
        $project = Project::factory()->create();
        $this->assertInstanceOf(User::class, $project->activities->first()->user);
    }
}
