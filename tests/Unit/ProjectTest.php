<?php

namespace Tests\Unit;

use App\Models\Project;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ProjectTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */

    use RefreshDatabase;

    public function test_path()
    {
        $this->signIn();
        $project = Project::factory()->create();
        $this->assertEquals('/projects/' . $project->id, $project->path());
    }

    public function test_user()
    {
        $this->signIn();
        $project = Project::factory()->create();
        $this->assertInstanceOf('App\Models\User', $project->user);
    }

    public function test_task()
    {
        $this->signIn();
        $project = Project::factory()->create();
        $task = $project->createTask('description');

        $this->assertCount(1, $project->tasks);
        $this->assertTrue($project->tasks->contains($task));
    }

    public function test_memo()
    {
        $this->signIn();
        $project = Project::factory()->create();
        $memo = $project->createMemo('description');

        $this->assertCount(1, $project->memos);
        $this->assertTrue($project->memos->contains($memo));
    }

    public function test_invite()
    {
        $this->signIn();
        $project = Project::factory()->create();
        $project->invite($user = User::factory()->create());
        $this->assertEquals($project->fresh()->team->skip(1)->get(1)->id, $user->id);
    }
}
