<?php

namespace Tests\Unit;

use App\Models\Project;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UserTest extends TestCase
{
    use RefreshDatabase;

   public function test_has_projects()
   {
        $user = User::factory()->create();

        $this->assertInstanceOf(Collection::class, $user->projects);
   }

   public function test_has_allProjects()
   {
       $user = User::factory()->create();
       $this->signIn($user);
       Project::factory()->create(['user_id' => $user->id]);
       $this->assertCount(1, $user->activeProjects());

       $newuser = User::factory()->create();
       $project = Project::factory()->create(['user_id' => $newuser->id]);
       $project->invite($user);
       $this->assertCount(2, $user->fresh()->activeProjects());
   }

   public function test_has_profilePage()
   {
       $this->withoutExceptionHandling();
       $user = User::factory()->create();
       $this->signIn($user);
       $this->get('/profiles/' . $user->id)->assertSee($user->name)->assertSee($user->email);
   }

   public function test_path()
   {
       $this->signIn();
       $this->assertEquals('/profiles/' .Auth()->id(), Auth()->user()->path());
   }
}
