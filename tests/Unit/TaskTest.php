<?php

namespace Tests\Unit;

use App\Models\Task;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class TaskTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    use RefreshDatabase;

    public function test_path()
    {
        $this->signIn();
        $task = Task::factory()->create();

        $this->assertEquals('/tasks/' . $task->id, $task->path());
    }

    public function test_project()
    {
        $this->signIn();
        $task = Task::factory()->create();
        $this->assertInstanceOf('App\Models\Project', $task->project);
    }
}
