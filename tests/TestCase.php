<?php

namespace Tests;

use App\Models\User;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Support\Facades\Auth;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    protected function signIn($user = null)
    {
        //? tak : nie
        $this->actingAs($user ?: User::factory()->create());
    }

    protected function signOut()
    {
        Auth::logout();
    }
}
