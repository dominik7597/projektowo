<?php

Breadcrumbs::for('projects', function ($trail) {
    $trail->push('Projekty', route('projects'));
});

Breadcrumbs::for('project', function ($trail, $project) {
    $trail->parent('projects');
    $trail->push($project->title, route('project', $project));
});

Breadcrumbs::for('activites', function ($trail, $project) {
    $trail->parent('project', $project);
    $trail->push('Aktywności', route('activities', $project));
});

Breadcrumbs::for('team', function ($trail, $project) {
    $trail->parent('project', $project);
    $trail->push('Użytkownicy', route('team', $project));
});

Breadcrumbs::for('team.user', function ($trail, $project, $user) {
    $trail->parent('team', $project);
    $trail->push($user->name, route('team.user', [$project, $user]));
});

Breadcrumbs::for('chat', function ($trail, $project) {
    $trail->parent('project', $project);
    $trail->push('Pokój czatowy', route('chat', $project));
});

Breadcrumbs::for('projects.search', function ($trail) {
    $trail->parent('projects');
    $trail->push('Wyszukiwarka projektów', route('projects.search'));
});

Breadcrumbs::for('projects.create', function ($trail) {
    $trail->parent('projects');
    $trail->push('Stwórz projekt', route('projects.create'));
});

Breadcrumbs::for('projects.edit', function ($trail, $project) {
    $trail->parent('project', $project);
    $trail->push('Edytuj projekt', route('projects.edit', $project));
});
