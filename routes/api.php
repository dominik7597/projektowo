<?php

use App\Http\Controllers\Api\ProjectController;
use App\Http\Controllers\Api\ProjectMemoController;
use App\Http\Controllers\Api\ProjectRequestController;
use App\Http\Controllers\Api\ProjectTaskController;
use App\Http\Controllers\Api\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::resource('/projects', ProjectController::class);
Route::get('/projects/search/{title}', [ProjectController::class, 'search']);
Route::get('/projects/{project}/team', [ProjectController::class, 'getTeam'])->name('get-team');

Route::resource('tasks', ProjectTaskController::class);
Route::resource('users', UserController::class);

Route::resource('/memos', ProjectMemoController::class);

Route::get('/requests', [ProjectRequestController::class, 'index']);
Route::get('/projects/{project}/request/{user}', [ProjectRequestController::class, 'show']);
Route::post('/projects/{project}/request/{user}', [ProjectRequestController::class, 'store']);
Route::delete('/projects/{project}/request/{user}', [ProjectRequestController::class, 'destroy']);

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
