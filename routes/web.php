<?php

use App\Http\Controllers\ConversationController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\InvitationController;
use App\Http\Controllers\ProjectTaskController;
use App\Http\Controllers\ProjectTeamController;
use App\Http\Controllers\ProjectRequestController;
use App\Http\Controllers\UserProfileController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProjectController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('login/google', [App\Http\Controllers\Auth\LoginController::class, 'redirectToGoogle'])->name('login.google');
Route::get('login/google/callback', [App\Http\Controllers\Auth\LoginController::class, 'handleGoogleCallback']);

Route::get('/', function () {
    if(auth()->user()) {
        return redirect('projects');
    }
    return view('main');
});

Route::group(['middleware' => 'auth'], function () {
    Route::get('/projects/search', [ProjectController::class, 'search'])->name('projects.search');
    Route::get('/projects/unactive', [ProjectController::class, 'unactive'])->name('projects.unactive');

    Route::resource('projects', ProjectController::class)
        ->name('index','projects')
        ->name('show','project')
        ->name('create', 'projects.create')
        ->name('edit', 'projects.edit');

    Route::resource('conversations', ConversationController::class);

    Route::get('/projects/{project}/activities', [ProjectController::class, 'activities'])->name('activities');
    Route::get('/projects/{project}/team/chat', [ProjectController::class, 'chat'])->name('chat');
    Route::get('/projects/{project}/team/users', [ProjectTeamController::class, 'users'])->name('team');
    Route::get('/projects/{project}/team/requests/{request?}', [ProjectTeamController::class, 'requests'])->name('requests');
    Route::patch('/projects/{project}/team/{user}', [ProjectTeamController::class, 'update']);
    Route::delete('/projects/{project}/team/{user}', [ProjectTeamController::class, 'destroy']);
    Route::get('/projects/{project}/team/{user}', [ProjectTeamController::class, 'show'])->name('team.user');

    Route::get('/projects/{project}/request/{user}', [ProjectRequestController::class, 'show']);
    Route::post('/projects/{project}/request/{user}', [ProjectRequestController::class, 'store']);
    Route::delete('/projects/{project}/request/{user}', [ProjectRequestController::class, 'destroy']);

    Route::post('/projects/{project}/tasks', [ProjectTaskController::class, 'store']);
    Route::patch('/tasks/{task}', [ProjectTaskController::class, 'update']);
    Route::delete('/tasks/{task}', [ProjectTaskController::class, 'destroy']);

    Route::get('/profiles', [UserProfileController::class, 'index']);
    Route::get('/profiles/{user}', [UserProfileController::class, 'show'])->name('profile');
    Route::get('/profiles/{user}/edit', [UserProfileController::class, 'edit']);
    Route::get('/profiles/{user}/activities', [UserProfileController::class, 'activities']);
    Route::get('/profiles/{user}/manage', [UserProfileController::class, 'manage']);
    Route::patch('/profiles/{user}/manage', [UserProfileController::class, 'adminUpdate']);
    Route::patch('/profiles/{user}', [UserProfileController::class, 'update']);
    Route::delete('/profiles/{user}', [UserProfileController::class, 'destroy']);

    Route::post('/projects/{project}/invitations', [InvitationController::class, 'store']);

    Route::get('/home', [HomeController::class, 'index'])->name('home');
});

Auth::routes();


