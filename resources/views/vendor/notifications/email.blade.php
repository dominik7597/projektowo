@component('mail::message')
{{-- Greeting --}}
@if (! empty($greeting))
# {{ $greeting }}
@else
@if ($level === 'error')
# @lang('Whoops!')
@else
# @lang('Witaj!')
@endif
@endif

{{-- Intro Lines --}}

Otrzymałeś ten E-Mail, ponieważ z twojego konta została wysłana prośba o zrestowanie hasła.

{{-- Action Button --}}
@isset($actionText)
<?php
    switch ($level) {
        case 'success':
        case 'error':
            $color = $level;
            break;
        default:
            $color = 'primary';
    }
?>
@component('mail::button', ['url' => $actionUrl, 'color' => $color])
{{ $actionText }}
@endcomponent
@endisset

{{-- Outro Lines --}}

Link restujący hasło wygaśnie w ciągu 60 min.

Jeśli nie wysyłałeś prośby o zmianę hasła, zignoruj tą wiadomość.

{{-- Salutation --}}
@if (! empty($salutation))
{{ $salutation }}
@else
@lang('Pozdrawiamy'),<br>
{{ config('app.name') }}
@endif

{{-- Subcopy --}}
@isset($actionText)
@slot('subcopy')
@lang(
    "Jeśli masz problem z kliknięciem przycisku \":actionText\" to skopiuj poniższy adres\n".
    'i wklej go do swojej przeglądarki:',
    [
        'actionText' => $actionText,
    ]
) <span class="break-all">[{{ $displayableActionUrl }}]({{ $actionUrl }})</span>
@endslot
@endisset
@endcomponent
