@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card text-center">
                    <div class="card-header">Witamy w projektowo!</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        Jeśli masz już konto, <a href="/login">zaloguj się</a>, w przeciwnym wypadku <a href="/register">zarejestruj się.</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
