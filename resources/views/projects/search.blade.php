@extends('layouts.app')
@section('content')
    {{ Breadcrumbs::render('projects.search') }}
    <div id="app">
        <project-search :user="{{ auth()->user() }}" :requests="{{ auth()->user()->request->pluck('pivot.project_id') }}"></project-search>
    </div>
@endsection
