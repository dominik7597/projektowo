@extends('layouts.app')
@section('content')
    {{ Breadcrumbs::render('projects.create') }}
    <p class="h4">Stwórz nowy projekt</p>
    <form method="POST" action="/projects" class="container pt-3">
        @csrf
        <div class="mb-3">
            <label class="form-label" for="title">Tytuł</label>
            <input type="text" class="form-control" name="title" required value="{{ old('title') }}">
        </div>

        <div class="mb-3">
            <label class="form-label" for="short_body">Krótszy opis</label>
            <textarea name="short_body" class="form-control" rows="2" required>{{ old('short_body') }}</textarea>
        </div>

        <div class="mb-3">
            <label class="form-label" for="body">Opis</label>
            <textarea class="form-control" name="body" id="editor">{{ old('body') }}</textarea>
        </div>

        @if($errors->any())
            <div class="mb-3" style="list-style: none">
                @foreach($errors->all() as $e)
                    <li class="alert-danger p-2 mb-1">{{ $e }}</li>
                @endforeach
            </div>
        @endif

        <div class="form-check mb-3">
            <input class="form-check-input" {{ old('is_public') ? 'checked' : '' }}
            onclick="check()" type="checkbox" name="is_public" value="1" id="is-public">
            <input type='hidden' name='is_public' value="0" id='is-public-hidden'>
            <label class="form-check-label" for="is-public">Ustaw projekt jako publiczny</label>
        </div>

        <div>
            <button type="submit" class="btn btn-outline-primary mr-2">Zapisz</button>
            <a href="/projects" class="btn btn-outline-secondary">Powrót</a>
        </div>
    </form>

    @push('styles')
         <style>
             .ck.ck-editor__main > div {
                 min-height: 300px;
             }
         </style>
    @endpush

    @push('scripts')
         <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
         <script src="{{ asset('ckeditor/translations/pl.js') }}"></script>
         <script>
             ClassicEditor
                 .create( document.querySelector( '#editor' ), {
                     removePlugins: ['CKFinderUploadAdapter','CKFinder', 'EasyImage',
                         'Image', 'ImageCaption', 'ImageStyle',
                         'ImageToolbar', 'ImageUpload', 'MediaEmbed'],
                     language: 'pl'
                 } )
                 .catch( error => {
                     console.error( error );
                 } );
         </script>

         <script>
             check();
             function check() {
                 document.getElementById('is-public-hidden').disabled = !!document.getElementById("is-public").checked;
             }
         </script>
    @endpush

@endsection
