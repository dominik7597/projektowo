@extends('projects.menu')
@section('menu-content')
    <ul class="ul1">
        @forelse($activeProjects as $pr)
            <li class="card">
                <div class="card-body">
                    <h3 class="card-title">
                        <a href="{{ $pr->path() }}">{{ $pr->title }}</a>
                    </h3>
                    <div class="card-text">{{ $pr->short_body }}</div>
                </div>
            </li>
        @empty
            <li>Nie posiadasz aktywnych projektów.</li>
        @endforelse
    </ul>
    <div class="d-flex justify-content-center mt-4">
        {{ $activeProjects->links() }}
    </div>
@endsection
