@can('update', $project)
    <div class="control is-expanded">
        <form action="{{ $project->path() }}" method="POST">
            @method('PATCH')
            @csrf
            <label class="form-label" for="body">Opis</label>
            <div>
                <textarea class="form-control has-fixed-size mb-3" name="memos" placeholder="Podziel się swoimi przemyśleniami!">{{ $project->memos}}</textarea>
            </div>
            <button type="submit" class="btn btn-outline-primary">Potwierdź</button>
        </form>
    </div>
@else
    <div class="control is-expanded">
        <form>
            <label class="label" for="body">Opis</label>
            <div>
                <textarea disabled="disabled" class="form-control has-fixed-size mb-3" cols="75" name="memos" placeholder="Podziel się swoimi przemyśleniami!">{{ $project->memos}}</textarea>
            </div>
            <button disabled="disabled" type="submit" class="btn btn-outline-primary">Potwierdź</button>
        </form>
    </div>
@endcan
