<div id="app">
    <memos role_prop="{{ $project->getTeamMemberRole(auth()->user()) }}"
           :project_id="{{ $project->id }}"
           :memos_prop="{{ $project->memos }}"
           :project_finished="{{ json_encode($project->finished) }}">
    </memos>
</div>
