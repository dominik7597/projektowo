@can('createTask', $project)
<div class="mt-md-5 mt-4">
    <form action="{{ $project->path() . '/tasks' }}" method="POST">
        @csrf
        <div>
            <input placeholder="Dodaj zadanie." name="description" class="form-control" required>
        </div>
        <div class="row mb-3 mt-md-3 mt-1">
            <div class="col-md-5">
                <label for="team" class="form-label">Zadanie dla</label>
                <select multiple name="team[]" class="form-control" id="team"></select>
            </div>
        </div>
    </form>
</div>
@if($errors->any())
    <div class="mb-3" style="list-style: none">
        @foreach($errors->all() as $e)
            <li class="alert-danger p-2 mb-1">{{ $e }}</li>
        @endforeach
    </div>
@endif
@endcan

@push('scripts')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script>
        $('#team').select2({
            ajax: {
                url: '{{ route('get-team', $project->id) }}',
                data: function (params) {
                    let query = {
                        search: params.term,
                        page: params.page || 1,
                    }
                    return query;
                }
            }
        });
    </script>
@endpush
