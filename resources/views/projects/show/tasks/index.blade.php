@forelse($project->tasks as $ta)
    <div class="row mb-3 align-items-center" style="{{ auth()->user()->can('update', $ta) ? "" : "pointer-events: none"}}">
        <div class="col-md-1 col-1-5" style="{{ auth()->user()->can('update', $ta) ? "" : "display: none"}}">
            <form action="{{ $ta->path() }}" method="POST">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-outline-danger btn-sm bi bi-x-square-fill ml-md-3"></button>
            </form>
        </div>
        <div class="col-md-11 col-10-5">
            <form action="{{ $ta->path() }}" method="POST">
                @method('PATCH')
                @csrf
                <div class="input-group" id="t{{ $ta->id }}">
                    <input name='description' value="{{ $ta->description }}" class="form-control" {{ $ta->achieved ? 'readonly' : '' }} required>
                    <div class="input-group-append" style="{{ auth()->user()->can('delete', $ta) ? "" : "display: none"}}">
                        <div class="input-group-text">
                            <input name="achieved" type="checkbox" onChange="this.form.submit()" {{ $ta->achieved ? 'checked' : '' }} class="checkbox">
                        </div>
                    </div>
                </div>
            </form>
            @if(count($team = $ta->team) > 0)
                <div class="mt-2">
                    @foreach($team as $tm)
                        @if($loop->iteration > 10)
                            @break
                        @endif
                        <a href="{{ $tm->path() }}">
                            <img src="{{ $tm->getAvatar() }}" class="rounded-circle" style="width: 20px; height: 20px" alt="{{ $tm->name }}">
                        </a>
                    @endforeach
                </div>
            @endif
        </div>
    </div>
@empty
    <div class="mb-3">
        Brak zadań.
    </div>
@endforelse

@push('styles')
    <style>
        .col-1-5 {
            width: 12.5%;
        }
        .col-10-5 {
            width: 87.5%;
        }
    </style>
@endpush

