<div class="control card p-4">
    <a href="{{ $project->path() . '/team/users' }}" class="stretched-link"></a>
    @if($project->team->skip(1)->first())
    <h5 class="card-title">Zespół:</h5>
    <div class="input-group d-flex justify-content-center" style="grid-gap: 10px">
        @foreach($project->team as $tm)
            @if($loop->index > 5)
                @break
            @elseif($tm->id !== auth()->id())
                <div>
                    <a href="{{ $tm->path() }}">
                        <img src="{{ $tm->getAvatar() }}" class="rounded-circle" style="width: 45px; height: 45px" alt="{{ $tm->name }}">
                    </a>
                </div>
            @endif
        @endforeach
    </div>
    @else
        Twój zespół jest pusty.
    @endif
</div>
