<div class="control card p-4">
    <div>
        Projekt utworzony {{ $project->created_at->format('d-m-Y') }} przez
        <a href="{{ $project->user->path() }}">
            {{ $project->user->name }}
        </a>
    </div>
    <div>
        Widoczność projektu:
        @if($project->is_public)
           publiczny
        @else
            niepubliczny
        @endif
    </div>
</div>
