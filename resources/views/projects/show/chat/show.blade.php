@extends('layouts.app')
@section('content')
    {{ Breadcrumbs::render('chat', $project) }}
    <div>
        <project-chat :project_prop="{{ $project->team }}"
                      :project_conversations_prop="{{ $project->conversations }}"
                      :project_id_prop="{{ $project->id }}"
                      :user_id_prop="{{ auth()->id() }}"
        ></project-chat>
    </div>
@endsection
