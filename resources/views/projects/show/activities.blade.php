@if(count($activities = $project->allActivities()->get()) > 0)
    <div class="control card p-4 ">
        <a href="{{ $project->path() . '/activities' }}" class="stretched-link"></a>
        <h5 class="card-title">Aktywności:</h5>
        <ul class="text-left p-0" style="list-style: none">
            @foreach($activities as $activity)
                @if($loop->index < 10)
                    <li class="d-flex p-1" style="grid-gap: 5px">
                        {{ $activity->mapDescription() }}
                        <div class="text-secondary"> - {{ $activity->created_at->diffForHumans() }}.</div>
                    </li>
                @endif
            @endforeach
        </ul>
    </div>
@else
    <div class="control card p-4">
        <a href="{{ $project->path() . '/activities' }}" class="stretched-link"></a>
        Nie posiadasz jeszcze żadnych aktywności.
    </div>
@endif
