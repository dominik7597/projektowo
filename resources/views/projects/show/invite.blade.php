@can('invite', $project)
    <div class="control card p-4">
        <h5 class="card-title text-center">Zaproś użytkownika:</h5>
        <form action="{{ $project->path() . '/invitations'}}" method="POST">
            <div class="input-group">
                @csrf
                <input type="email" name="email" class="form-control" placeholder="Adres E-Mail">
                <div class="input-group-append">
                    <button type="submit" class="btn btn-outline-primary">Zaproś</button>
                </div>
            </div>
        </form>
        <div>
            @if($errors->any())
                <div class="mt-2" style="list-style: none; color: red">
                    @foreach($errors->all() as $e)
                        <li>{{ $e }}</li>
                    @endforeach
                </div>
            @endif
        </div>
    </div>
@endcan
