@can('manage', $project)
    <div class="control card p-4">
        <div class="d-flex justify-content-center" style="grid-gap: 10px">
            @if($project->finished === null)
                <a href="{{ $project->path() . "/edit" }}" type="submit" class="btn btn-outline-primary"><i class="fas fa-edit"></i> Edytuj</a>
                <form action="{{ $project->path() }}" method="POST">
                    @csrf
                    @method('PATCH')
                    <input type="hidden" name="finished" value="1">
                    <button type="submit" class="btn btn-outline-success"><i class="fas fa-check"></i> Zakończ</button>
                </form>
            @else
                <form action="{{ $project->path() }}" method="POST">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-outline-danger"><i class="far fa-trash-alt"></i> Usuń</button>
                </form>
                <form action="{{ $project->path() }}" method="POST">
                    @csrf
                    @method('PATCH')
                    <input type="hidden" name="finished" value="{{null}}">
                    <button type="submit" class="btn btn-outline-success"><i class="fas fa-undo"></i> Wznów</button>
                </form>
            @endif
        </div>
    </div>
@endcan
