@extends('layouts.app')
@section('content')
    {{ Breadcrumbs::render('activites', $project) }}
    @if(count($activities = $project->allActivities()->paginate(12)) > 0)
        <div class="control card p-4">
            <h5 class="card-title text-center">Aktywności:</h5>
            <ul class="text-left p-0 mb-0" style="list-style: none">
                @foreach($activities as $activity)
                    <li class="d-md-flex p-2 mt-md-0 mt-4 {{ $loop->last ? 'mb-0' : 'mb-2' }}" style="grid-gap: 5px">
                        @include("projects.activities.{$activity->description}")
                        <div class="text-secondary float-md-none float-right">{{ $activity->created_at->diffForHumans() }}.</div>
                    </li>
                @endforeach
            </ul>
        </div>
        <div class="d-flex justify-content-center mt-3">
            {{ $activities->links() }}
        </div>
    @else
        <div class="control card p-4">
            Nie posiadasz jeszcze żadnych aktywności.
        </div>
    @endif
@endsection

