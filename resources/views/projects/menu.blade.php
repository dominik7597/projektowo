@extends('layouts.app')
@section('content')
    {{ Breadcrumbs::render('projects') }}
    <link href="{{ asset('css/index.blade.css') }}" rel="stylesheet">
    <div class="d-flex mb-3 d1">
        <a href="/projects/create" class="btn btn-outline-primary"><i class="fas fa-plus"></i> Nowy projekt</a>
        <a href="/projects/search" class="btn btn-outline-primary"><i class="fas fa-search"></i> Przeglądaj projekty</a>
    </div>

    <nav class="nav nav-pills nav-justified mb-4">
        <a class="nav-item nav-link border border-right-0 rounded-0 {{ ('/' . request()->path() === '/projects') ? 'active' : '' }}" href="/projects">Aktywne projekty</a>
        <a class="nav-item nav-link border border-left-0 rounded-0 {{ ('/' . request()->path() === '/projects/unactive') ? 'active' : '' }}" href="/projects/unactive">Ukończone projekty</a>
    </nav>

    @yield('menu-content')
@endsection
