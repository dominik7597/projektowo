@extends('layouts.app')
@section('content')
    <link rel="stylesheet" href="https://unpkg.com/@icon/bootstrap-icons/bootstrap-icons.css">
    {{ Breadcrumbs::render('project', $project) }}
    <div class="card p-4 text-center mb-3">
        <h1 class="title">{{ $project->title }}</h1>
        <div class="mb-5">{{ $project->short_body }}</div>
        <div>{!! html_entity_decode($project->body) !!}</div>
    </div>
    <div class="row">
        <div class="col-md-8">
            @include('projects.show.tasks.index')
            @include('projects.show.tasks.create')
            @include('projects.show.memos.index')
        </div>
        <div class="col-md-4 mt-md-0 mt-2 text-center">
            <div class="mb-2">
                @include('projects.show.activities')
            </div>
            <div class="mb-2">
                @include('projects.show.team')
            </div>
            <div class="mb-2">
                @include('projects.show.chat')
            </div>
            <div class="mb-2">
                @include('projects.show.information')
            </div>
            @include('projects.show.manage')
        </div>
    </div>
@endsection
