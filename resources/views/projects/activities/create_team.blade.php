@if($activity->user->id === Auth::user()->id)
    Dołączyłeś do projektu
@else
    <div>
        Użytkownik
        <a href="{{ $activity->user->path() }}">
            {{ Str::of($activity->user->name)->limit(30) }}
        </a>
        dołączył do projektu
    </div>
@endif
