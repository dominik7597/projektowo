@if($activity->user->id === Auth::user()->id)
    Usunąłeś zadanie {{ Str::of($activity->subject_name)->limit(150) }}
@else
    <div>
        <a href="{{ $activity->user->path() }}">{{ $activity->user->name }}</a>
        usunął zadanie {{ Str::of($activity->subject_name)->limit(150) }}
    </div>
@endif
