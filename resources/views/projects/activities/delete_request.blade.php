@if($activity->user->id === Auth::user()->id)
    <div>
        Prośba o dołączenie do projektu {{ Str::of($activity->subject_name)->limit(30) }} została anulowana.
    </div>
@endif
