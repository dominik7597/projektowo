@if($activity->user->id === Auth::user()->id)
    Zakończyłeś zadanie {{ $activity->subject->description }}
@else
    <div>
        <a href="{{ $activity->user->path() }}">{{ $activity->user->name }}</a> zakończył zadanie {{ $activity->subject->description }}
    </div>
@endif
