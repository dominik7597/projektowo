 @if($activity->user->id === Auth::user()->id)
     <div>
         Utworzyłeś projekt
         @if($activity->subject->path() !== '/' . request()->path())
             <a href="{{ $activity->subject->path() }}"> {{ Str::of($activity->subject->title)->limit(150) }}</a>
         @endif
     </div>
@else
     <div>
         <a href="{{ $activity->user->path() }}">{{ $activity->user->name }}</a> utworzył projekt
         @if($activity->subject->path() !== '/' . request()->path())
             <a href="{{ $activity->subject->path() }}"> {{ Str::of($activity->subject->title)->limit(150) }}</a>
         @endif
     </div>
@endif

