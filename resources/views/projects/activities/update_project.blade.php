@if($activity->user->id === Auth::user()->id)
    <div>
        Zaktualizowałeś projekt
        @if($activity->subject->path() !== '/' . request()->path())
            <a href="{{ $activity->subject->path() }}"> {{ $activity->subject->title }}</a>
        @endif
    </div>
@else
    <div>
        <a href="{{ $activity->user->path() }}">{{ $activity->user->name }}</a> zaktualizował projekt
    </div>
@endif
