@if($activity->user->id === Auth::user()->id)
    Opuściłeś projekt
@else
    <div>
        Użytkownik
        <a href="{{ $activity->user->path() }}">
            {{ Str::of($activity->user->name)->limit(30) }}
        </a>
        opuścił projekt
    </div>
@endif
