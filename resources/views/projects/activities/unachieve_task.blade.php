@if($activity->user->id === Auth::user()->id)
    Aktywowałeś ponownie zadanie {{ $activity->subject->description }}
@else
    <div>
        <a href="{{ $activity->user->path() }}">{{ $activity->user->name }}</a> aktywował zadanie {{ $activity->subject->description }}
    </div>
@endif


