@if($activity->user->id === Auth::user()->id)
    <div>
        Zaktualizowałeś zadanie
        <a href="{{ $activity->subject->project->path()."tasks#t".$activity->subject->id }}">
            {{ Str::of($activity->subject->description)->limit(150) }}
        </a>
    </div>
@else
    <div>
        <a href="{{ $activity->user->path() }}">{{ $activity->user->name }}</a>
        zaktualizował zadanie
        <a href="{{ $activity->subject->project->path()."tasks#t".$activity->subject->id }}">
            {{ Str::of($activity->subject->description)->limit(150) }}
        </a>
    </div>
@endif
