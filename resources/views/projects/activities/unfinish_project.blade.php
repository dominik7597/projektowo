@if($activity->user->id === Auth::user()->id)
    <div>
        Wznowiłeś projekt
        @if($activity->subject->path() !== '/' . request()->path())
            <a href="{{ $activity->subject->path() }}"> {{ $activity->subject->title }}</a>
        @endif
    </div>
@else
    <div>
        <a href="{{ $activity->user->path() }}">{{ $activity->user->name }}</a> wznowił projekt
    </div>
@endif
