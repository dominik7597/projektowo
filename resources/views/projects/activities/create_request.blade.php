@if($activity->user->id === Auth::user()->id)
    <div>
        Utworzyłeś prośbę o dołączenie do projektu {{ Str::of($activity->subject->project->title)->limit(35) }}
    </div>
@else
    <div>
        <a href="{{ $activity->user->path() }}">{{ $activity->user->name }}</a> chce dołączyć do projektu.
        <a href="{{ $activity->subject->project->path()."/team/requests/" . $activity->subject->id . "/#r" . $activity->subject->id }}">
        <i class="fas fa-eye"></i></a>
    </div>
@endif
