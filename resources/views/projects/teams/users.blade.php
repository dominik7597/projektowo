@extends('projects.teams.show')
@section('menu-content')
    <div class="control card p-4 mb-3">
        <link href="{{ asset('css/team.blade.css') }}" rel="stylesheet">
        @if($project->team->skip(1)->first())
            <div class="input-group">
                @foreach($project->team as $tm)
                    @if($tm->id !== auth()->id())
                        <li class="nav-item dropright li1">
                            <a id="navbarDropdown" class="nav-link text-center" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img src="{{ $tm->getAvatar() }}" class="rounded-circle" style="width: 45px; height: 45px" alt="{{ $tm->name }}">
                                <p>{{ $tm->name }}</p>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ $tm->path() }}">
                                    {{ __('Profil') }}
                                </a>
                                @can('manage', $project)
                                    @if($tm->id !== $project->user_id)
                                        <a class="dropdown-item" href="{{ $project->path() . '/team/' . $tm->id }}">
                                            {{ __('Uprawnienia') }}
                                        </a>
                                        <a class="dropdown-item"
                                           onclick="event.preventDefault();
                                               document.getElementById({{ $tm->id }}).submit();">
                                            {{ __('Usuń') }}
                                        </a>
                                    @endif
                                @endcan
                                <form id="{{ $tm->id }}" action="{{ $project->path() . '/team/' . $tm->id }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                </form>
                            </div>
                        </li>
                    @endif
                @endforeach
            </div>
        @else
            <h5 class="mb-0">Twój zespół jest pusty.</h5>
        @endif
    </div>
@endsection
