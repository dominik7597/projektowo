@extends('layouts.app')
@section('content')
    {{ Breadcrumbs::render('team', $project) }}
    @include('projects.show.invite')
    <nav class="navbar navbar-expand navbar-light bg-white border border-bottom-0 mt-3">
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item {{ ('/' . request()->path() === $project->path() . '/team/users') ? 'active' : '' }}">
                    <a class="nav-link" href="{{ $project->path() }}/team/users">Użytkownicy <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item {{ ('/' . request()->path() === $project->path() . '/team/requests') ? 'active' : '' }}">
                    <a class="nav-link" href="{{ $project->path() }}/team/requests">Oczekujący <span class="sr-only">(current)</span></a>
                </li>
            </ul>
        </div>
    </nav>
    @yield('menu-content')
@endsection
