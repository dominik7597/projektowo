@extends('layouts.app')
@section('content')
    {{ Breadcrumbs::render('team.user', $project, $user) }}
    <div class="card flex-fill justify-content-center align-items-center p-3">
        <div>
            <img src="{{ $user->getAvatar() }}" class="rounded-circle" style="width: 50px" alt="{{ $user->name }}">
        </div>
        <p class="h5 mt-2 mb-0">{{ $user->name }}</p>
    </div>
    <div>
        <form action="{{ $project->path() . '/team/' . $user->id }}" method="POST" id="role">
            @csrf
            @method('PATCH')
            <div class="form-group mt-3">
                <label>Projekt:</label>
                <select class="form-control" name="role">
                    <option value="view" {{ ( 'view' === $project->getTeamMemberRole($user)) ? 'selected' : '' }}>Prawo do przeglądania</option>
                    <option value="update"  {{ ( 'update' === $project->getTeamMemberRole($user)) ? 'selected' : '' }}>Prawo do edycji</option>
                    <option value="manage"  {{ ( 'manage' === $project->getTeamMemberRole($user)) ? 'selected' : '' }}>Prawo do zarządzania</option>
                </select>
            </div>
{{--            <div class="form-group mt-3">--}}
{{--                <label>Zadania:</label>--}}
{{--                <select class="form-control" form="role" name="role" id="role">--}}
{{--                    <option value="view">Prawo do przeglądania</option>--}}
{{--                    <option value="update">Prawo do edycji</option>--}}
{{--                    <option value="manage">Prawo do zarządzania</option>--}}
{{--                </select>--}}
{{--            </div>--}}
{{--            <div class="form-group mt-3">--}}
{{--                <label>Notatki:</label>--}}
{{--                <select class="form-control" form="role" name="role" id="role">--}}
{{--                    <option value="view">Prawo do przeglądania</option>--}}
{{--                    <option value="update">Prawo do edycji</option>--}}
{{--                    <option value="manage">Prawo do zarządzania</option>--}}
{{--                </select>--}}
{{--            </div>--}}
            <input type="submit" value="Zatwierdź" class="btn btn-outline-success">
        </form>
    </div>
@endsection
