@extends('projects.teams.show')
@section('menu-content')
    <link rel="stylesheet" href="https://unpkg.com/@icon/bootstrap-icons/bootstrap-icons.css">
        <div class="control card p-3 mb-0">
            @if($project->request->first())
                <div class="input-group">
                    @foreach($requests as $rq)
                        <div class="col-md-10-5 col-7 d-md-flex p-3" style="grid-gap: 5px" id="r{{ $rq->pivot->id }}">
                            <a href="{{ $rq->path() }}">{{ $rq->name }} </a>wysłał prośbę o dołączenie do projektu
                            <div class="text-secondary">{{ $rq->pivot->created_at->diffForHumans() }}.</div>
                        </div>
                        @can('update', $project)
                            <div class="col-md-1-5 col-5 d-flex align-items-center justify-content-end p-0" style="grid-gap: 10px">
                                <form action="{{ $project->path() . '/invitations'}}" method="POST">
                                    <div class="input-group">
                                        @csrf
                                        @method('POST')
                                        <input type="hidden" name="email" value="{{ $rq->email }}">
                                        <button type="submit" class="btn btn-outline-success"><i class="fas fa-check-square"></i></button>
                                    </div>
                                </form>
                                <form action="{{ $project->path() . '/request/' . $rq->id }}" method="POST">
                                    <div class="input-group">
                                        @csrf
                                        @method('DELETE')
                                        <input type="hidden" name="email" value="{{ $rq->email }}">
                                        <button type="submit" class="btn btn-outline-danger bi bi-x-square-fill mr-3"></button>
                                    </div>
                                </form>
                            </div>
                        @endcan
                    @endforeach
                </div>
                <div class="d-flex justify-content-center mt-3">
                    {{ $requests->links() }}
                </div>
            @else
                <h5 class="mb-0 text-center">Brak oczekujących.</h5>
            @endif
        </div>
@endsection

@push('styles')
    <style>
        :target {
            background-color: aliceblue;
        }
        .pagination {
            margin-bottom: 0;
        }
        .col-md-1-5 { width: 12.5%; }

        .col-md-10-5 { width: 87.5%; }
    </style>
@endpush

@push('scripts')
    <script>
        $(window.location.hash).mouseover(function(){
            $(this).css("background-color", "white");
        });
        let x = document.getElementById(window.location.hash.split('#').pop());
    </script>
@endpush

