@extends('profiles.menu')
@section('menu-content')
    <div class="card flex-fill justify-content-center align-items-center p-5">
        <div>
            <img src="{{ $user->getAvatar() }}" class="rounded-circle" style="width: 100px; height: 100px" alt="{{ $user->name }}">
        </div>
        <p class="h3 mt-3">Nazwa: {{ $user->name }}</p>
        <div>Adres E-Mail: {{ $user->email }}</div>
    </div>
@endsection
