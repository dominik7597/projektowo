@extends('layouts.app')
@section('content')
    <div id="app">
        <profile-search :user="{{ auth()->user() }}" :link="{{ json_encode(Storage::disk('s3')->url('avatars/')) }}"></profile-search>
    </div>
@endsection
