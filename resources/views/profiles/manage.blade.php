@extends('profiles.menu')
@section('menu-content')
    <div class="card flex-fill">
        <div class="d-md-flex p-md-0 p-3 text-center justify-content-center align-items-center" style="grid-gap: 30px; flex: 1">
            @if($user->isAdmin())
                <form action="{{ $user->path() }}./manage" method="POST">
                    @method('PATCH')
                    @csrf
                    <input hidden name="role" value="user">
                    <button type="submit" class="btn btn-outline-success mb-md-0 mb-2"><i class="fas fa-user-minus"></i> Usuń administratora</button>
                </form>
            @else
                <form action="{{ $user->path() }}./manage" method="POST">
                    @method('PATCH')
                    @csrf
                    <input hidden name="role" value="admin">
                    <button type="submit" class="btn btn-outline-success mb-md-0 mb-2"><i class="fas fa-user-plus"></i> Dodaj administratora</button>
                </form>
            @endif
                @if($user->isActive())
                    <form action="{{ $user->path() }}./manage" method="POST">
                        @csrf
                        @method('PATCH')
                        <input hidden name="active" value="0">
                        <button type="submit" class="btn btn-outline-danger mb-md-0 mb-2"><i class="fas fa-lock"></i> Zbanuj użytkownika</button>
                    </form>
                @else
                    <form action="{{ $user->path() }}./manage" method="POST">
                        @csrf
                        @method('PATCH')
                        <input hidden name="active" value="1">
                        <button type="submit" class="btn btn-outline-danger mb-md-0 mb-2"><i class="fas fa-unlock"></i> Odbanuj użytkownika</button>
                    </form>
                @endif
            <form action="{{ $user->path() }}" method="POST">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-outline-danger"><i class="fas fa-user-slash"></i> Usuń użytkownika</button>
            </form>
        </div>
    </div>
@endsection
