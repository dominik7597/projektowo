@extends('profiles.menu')
@section('menu-content')
    <div class="card flex-fill justify-content-center p-3">
        <form action="{{ $user->path() }}" method="POST" enctype="multipart/form-data">
            @method('PATCH')
            @csrf
            <div class="container">
                <div class="text-center mb-3">
                    <img src="{{ $user->getAvatar() }}" class="rounded-circle" style="width: 100px" alt="{{ $user->name }}">
                </div>
                <div class="row justify-content-center mb-3">
                    <div class="col-md-4">
                        <input type="file" id="avatar_image" name="avatar_image">
                    </div>
                </div>

                <div class="row align-items-center mb-3">
                    <div class="col-md-2">
                        <label class="mb-0" for="name">Nazwa</label>
                    </div>
                    <div class="col-md-10">
                        <input type="text" class="form-control" id="name" name="name" value="{{ $user->name }}" required>
                    </div>
                </div>

                <div class="row align-items-center mb-3">
                    <div class="col-md-2">
                        <label class="mb-0" for="email">E-Mail</label>
                    </div>
                    <div class="col-md-10">
                        <input type="email" class="form-control" id="email" name="email" value="{{ $user->email }}" required>
                    </div>
                </div>

                <div class="row align-items-center mb-3">
                    <div class="col-md-2">
                        <label class="mb-0" for="password">Hasło</label>
                    </div>
                    <div class="col-md-10">
                        <input type="password" class="form-control" id="password" name="password">
                    </div>
                </div>

                <div class="row align-items-center justify-content-end">
                    <div class="col-md-10">
                        <button type="submit" class="btn btn-primary">Zatwierdź zmiany</button>
                    </div>
                </div>
            </div>

            @if($errors->any())
                <div class="form-group row justify-content-end">
                    <div class="col-sm-10" style="list-style: none; color: red">
                        @foreach($errors->all() as $e)
                            <li>{{ $e }}</li>
                        @endforeach
                    </div>
                </div>
            @endif
        </form>
    </div>
@endsection
