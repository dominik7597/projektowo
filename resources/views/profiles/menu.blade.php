@extends('layouts.app')
@section('content')
    <link href="{{ asset('css/profile.blade.css') }}" rel="stylesheet">
    <div class="row">
        <div class="list-group p-md-5 div1">
            <a href="{{ $user->path() }}"
               class="list-group-item list-group-item-action {{ ('/' . request()->path() === $user->path()) ? 'active' : '' }}">Dane</a>
            @can('update', $user)
                <a href="{{ $user->path() }}/edit" class="list-group-item list-group-item-action {{ ('/' . request()->path() === $user->path() . '/edit') ? 'active' : '' }}">Ustawienia</a>
                <a href="{{ $user->path() }}/activities"
                   class="list-group-item list-group-item-action {{ ('/' . request()->path() === $user->path() . '/activities') ? 'active' : '' }}">Aktywności</a>
            @endcan
            @can('delete', $user)
                <a href="{{ $user->path() }}/manage" class="list-group-item list-group-item-action {{ ('/' . request()->path() === $user->path() . '/manage') ? 'active' : '' }}">Zarządzaj</a>
            @endcan
        </div>
        @yield('menu-content')
    </div>
@endsection
