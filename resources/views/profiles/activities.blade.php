@extends('profiles.menu')
@section('menu-content')
    <div class="card flex-fill justify-content-center p-5">
        @forelse($activities = $user->getActivities()->paginate(25) as $activity)
            <li style="list-style: none">
                <div class="d-md-flex mt-md-0 mt-4 justify-content-between">
                    @include("projects.activities.{$activity->description}")

                    <div class="text-secondary">{{ $activity->created_at->diffForHumans() }}.</div>
                </div>
            </li>
        @empty
            <p class="text-center mt-4">Nie posiadasz jeszcze żadnych aktywności.</p>
        @endforelse
        <div class="d-flex justify-content-center mt-4">
            {{ $activities->links() }}
        </div>
    </div>
@endsection

@push('styles')
    <style>
        .pagination {
            margin-bottom: 0;
        }
    </style>
@endpush
