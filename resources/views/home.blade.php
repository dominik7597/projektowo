@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card text-center">
                <div class="card-header">Witaj w projektowo!</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <a href="/projects/create">Już teraz rozpocznij swój pierwszy projekt.</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
