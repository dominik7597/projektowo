<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Relations\Pivot;

class ProjectTeam extends pivot
{
    protected $with = ['user'];

    public $incrementing = true;

    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function activities()
    {
        return $this->morphMany(Activity::class, 'subject')->latest();
    }
}
