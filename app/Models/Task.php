<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use HasFactory;

    protected $with = ['project', 'team'];

    protected $fillable = ['description', 'achieved'];

    protected $touches = ['project'];

    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    public function path()
    {
        return '/tasks/' . $this->id;
    }

    public function activities()
    {
        return $this->morphMany(Activity::class, 'subject')->latest();
    }

    public function invite(User $user)
    {
        return $this->team()->attach($user);
    }

    public function team()
    {
        return $this->belongsToMany(User::class, 'task_team')->withTimestamps();
    }
}
