<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Laravel\Scout\Searchable;

class User extends Authenticatable
{
    use HasFactory, Notifiable, Searchable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'avatar_image',
        'last_login_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function projects()
    {
        return $this->hasMany(Project::class);
    }

    public function team()
    {
        return $this->belongsToMany(Project::class, 'project_team')
            ->withPivot('role')
            ->withTimestamps();
    }

    public function request()
    {
        return $this->belongsToMany(Project::class, 'project_request')
            ->using(ProjectRequest::class)
            ->withTimestamps()
            ->withPivot( 'status');
    }

    public function activeProjects()
    {
        return $this->team->where('finished', null);
    }

    public function finishedProjects()
    {
        return $this->team->where('finished');
    }

    public function path()
    {
        return '/profiles/' . $this->id;
    }

    public function setPasswordAttribute($password)
    {
        return $this->attributes['password'] = Hash::needsRehash($password) ? Hash::make($password) : $password;
    }

    public function getAvatar()
    {
        if($this->avatar_image === null) {
            return 'https://www.gravatar.com/avatar/'.md5($this->email).'?s=45';
        }
        else {
            if(str_contains($this->avatar_image, 'https://lh3.googleusercontent.com/')) {
                return $this->avatar_image;
            }

            return Storage::disk('s3')->url('avatars/' . $this->avatar_image);
        }
    }

    public function getActivities()
    {
        $userActivities = Activity::where([['user_id', $this->id], ['subject_type', ProjectRequest::class]])->get();

        $projectsActivities = Activity::whereIn('project_id', $this->activeProjects()->pluck('id'))->get();
        $projectsFinishedActivities = Activity::whereIn('project_id', $this->finishedProjects()->pluck('id'))->where('description', ['finish_project', 'unfinish_project'])->get();
        return $projectsActivities->merge($userActivities)->merge($projectsFinishedActivities)->sortByDesc('created_at');
    }

    public function isAdmin()
    {
        return $this->role === 'admin';
    }

    public function isActive()
    {
        return $this->active;
    }
}
