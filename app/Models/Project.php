<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Project extends Model
{
    use HasFactory, Searchable;

    protected $fillable = ['title', 'short_body', 'body', 'is_public', 'finished', 'user_id'];

    public function path()
    {
        return '/projects/' . $this->id;
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function tasks()
    {
        return $this->hasMany(Task::class);
    }

    public function memos()
    {
        return $this->hasMany(Memo::class);
    }

    public function getUserTasks($user)
    {
        return $this->tasks->where('user_id', $user->id);
    }

    public function conversations()
    {
        return $this->hasMany(Conversation::class)->with('user');
    }

    public function createTask($description)
    {
        return $this->tasks()->create(compact('description'));
    }

    public function createMemo($description)
    {
        return $this->memos()->create(compact('description'));
    }

    public function createConversation($message, $user_id)
    {
        return $this->conversations()->create(compact('message', 'user_id'));
    }

    public function activities()
    {
        return $this->morphMany(Activity::class, 'subject')->latest();
    }

    public function allActivities() {
        return Activity::where('project_id', $this->id)->latest();
    }

    public function invite(User $user)
    {
        return $this->team()->attach($user);
    }

    public function finish()
    {
        $this->finished=now();
        $this->save();
    }

    public function team()
    {
        return $this->belongsToMany(User::class, 'project_team')
            ->using(ProjectTeam::class)
            ->withPivot('role')
            ->withTimestamps();
    }

    public function hasUser($tm_id)
    {
        foreach ($this->team as $tm) {
            if($tm->id == $tm_id) {
                return true;
            }
        }
        return false;
    }

    public function request()
    {
        return $this->belongsToMany(User::class, 'project_request')
            ->using(ProjectRequest::class)
            ->withPivot('id', 'status')
            ->withTimestamps();
    }

    public function getTeamMember($user)
    {
        return $this->team->where('pivot.user_id',$user->id)->first();
    }

    public function getTeamMemberRole($user)
    {
        if($user->role === 'admin') {
            return 'manage';
        }
        else {
            $tm = $this->getTeamMember($user);
            if($tm) {
                return $tm->pivot->role;
            }
        }
    }

    public function toSearchableArray()
    {
        $array = $this->toArray();

        $array['team'] = $this->team->map(function ($data) {
            return $data['id'];
        })->toArray();

        if($this->finished)
        {
            $array['is_finished'] = 1;
        }
        else {
            $array['is_finished'] = 0;
        }

        return $array;
    }
}
