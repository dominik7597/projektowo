<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    use HasFactory;

    protected $with = [
        'user',
        'subject',
    ];

    protected $fillable = ['user_id', 'project_id', 'description', 'subject_id', 'subject_name'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function subject()
    {
        return $this->morphTo();
    }

    public function mapDescription()
    {
        $tab = ['create_project', 'update_project', 'finish_project', 'unfinish_project',
            'create_task', 'update_task', 'achieve_task', 'unachieve_task', 'delete_task',
            'create_team', 'delete_team', 'create_request', 'delete_request'];
        $tab2 = ['Utworzono projekt', 'Zaktualizowano projekt', 'Zakończono projekt', 'Wznowiono projekt',
            'Utworzono zadanie', 'Zaktualizowano zadanie', 'Zakończono zadanie', 'Wznowiono zadanie', 'Usunięto zadanie',
            'Nowy użytkownik', 'Usunięty użytkownik', 'Nowa prośba', 'Prośba anulowana'];
        foreach ($tab as $key=>$a) {
            if($this->description === $a) {
                return $tab2[$key];
            }
        }
        return $this->description;
    }
}
