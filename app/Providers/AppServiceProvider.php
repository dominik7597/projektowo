<?php

namespace App\Providers;

use App\Models\Project;
use App\Models\ProjectRequest;
use App\Models\ProjectTeam;
use App\Models\Task;
use App\Observers\ProjectObserver;
use App\Observers\ProjectRequestObserver;
use App\Observers\ProjectTeamObserver;
use App\Observers\TaskObserver;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        ProjectRequest::observe(ProjectRequestObserver::class);
        ProjectTeam::observe(ProjectTeamObserver::class);
        Project::observe(ProjectObserver::class);
        Task::observe(TaskObserver::class);
        Carbon::setLocale(config('app.locale'));

        if (config('app.env') === 'production') {
            \URL::forceScheme('https');
        }

        Schema::defaultStringLength(191);

        Paginator::useBootstrap();

        if (!Collection::hasMacro('paginate')) {

            Collection::macro('paginate',
                function ($perPage = 15, $page = null, $options = []) {
                    $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
                    return (new LengthAwarePaginator(
                        $this->forPage($page, $perPage)->values()->all(), $this->count(), $perPage, $page, $options))
                        ->withPath('');
                });
        }
    }
}
