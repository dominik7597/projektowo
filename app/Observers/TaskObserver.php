<?php

namespace App\Observers;

use App\Models\Activity;
use App\Models\Task;
use Illuminate\Support\Facades\Auth;

class TaskObserver
{
    /**
     * Handle the Task "created" event.
     *
     * @param Task $task
     * @return void
     */
    public function created(Task $task)
    {
        auth()->id() === null ? $user_id = $task->project->user_id : $user_id = auth()->id();

        $task->activities()->create([
            'user_id' => $user_id,
            'project_id' => $task->project->id,
            'description' => 'create_task',
        ]);
    }

    /**
     * Handle the Task "updated" event.
     *
     * @param Task $task
     * @return void
     */
    public function updated(Task $task)
    {
        auth()->id() === null ? $user_id = $task->project->user_id : $user_id = auth()->id();

        if(request('achieved')) {
            if($task->getOriginal('achieved')) {
                $task->activities()->create([
                    'user_id' => $user_id,
                    'project_id' => $task->project->id,
                    'description' => 'update_task'
                ]);
            }
            else {
                $task->activities()->create([
                    'user_id' => $user_id,
                    'project_id' => $task->project->id,
                    'description' => 'achieve_task'
                ]);
            }
        } else {
            if($task->getOriginal('achieved')) {
                $task->activities()->create([
                    'user_id' => $user_id,
                    'project_id' => $task->project->id,
                    'description' => 'unachieve_task'
                ]);
            }
            else {
                $task->activities()->create([
                    'user_id' => $user_id,
                    'project_id' => $task->project->id,
                    'description' => 'update_task'
                ]);
            }
        }
    }

    /**
     * Handle the Task "deleted" event.
     *
     * @param Task $task
     * @return void
     */
    public function deleted(Task $task)
    {
        auth()->id() === null ? $user_id = $task->project->user_id : $user_id = auth()->id();

        $task->activities()->delete();

        $task->activities()->create([
            'user_id' => $user_id,
            'project_id' => $task->project->id,
            'description' => 'delete_task',
            'subject_name' => $task->project->title
        ]);
    }
}
