<?php


namespace App\Observers;


use App\Models\ProjectTeam;

class ProjectTeamObserver
{
    public function created(ProjectTeam $projectTeam)
    {
        if($projectTeam->project->user->id!==$projectTeam->user->id) {
            $projectTeam->activities()->create([
                'user_id' => $projectTeam->user_id,
                'project_id' => $projectTeam->project_id,
                'description' => 'create_team',
            ]);
        }
    }

    public function deleting(ProjectTeam $projectTeam)
    {
        $projectTeam->activities()->create([
            'user_id' => $projectTeam->user_id,
            'project_id' => $projectTeam->project_id,
            'description' => 'delete_team'
        ]);
    }
}
