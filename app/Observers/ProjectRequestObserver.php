<?php

namespace App\Observers;

use App\Models\Activity;
use App\Models\ProjectRequest;

class ProjectRequestObserver
{
    public function created(ProjectRequest $projectRequest)
    {
        $projectRequest->activities()->create([
            'user_id' => $projectRequest->user_id,
            'project_id' => $projectRequest->project_id,
            'description' => 'create_request',
        ]);
    }
}
