<?php

namespace App\Observers;

use App\Events\ProjectCreated;
use App\Models\Project;

class ProjectObserver
{
    /**
     * Handle the Project "created" event.
     *
     * @param Project $project
     * @return void
     */
    public function created(Project $project)
    {
        $project->activities()->create([
            'user_id' => $project->user->id,
            'project_id' => $project->id,
            'description' => 'create_project'
        ]);

        $project->team()->attach($project->user);
        $project->getTeamMember($project->user)->pivot->role = 'manage';
        $project->getTeamMember($project->user)->pivot->save();

        broadcast(new ProjectCreated($project))->toOthers();
    }

    /**
     * Handle the Project "updated" event.
     *
     * @param Project $project
     * @return void
     */
    public function updated(Project $project)
    {
        auth()->id() === null ? $user_id = $project->user_id : $user_id = auth()->id();

        if(request('finished')) {
            if($project->getOriginal('finished')) {
                $project->activities()->create([
                    'user_id' => $user_id,
                    'project_id' => $project->id,
                    'description' => 'update_project'
                ]);
            }
            else {
                $project->activities()->create([
                    'user_id' => $user_id,
                    'project_id' => $project->id,
                    'description' => 'finish_project'
                ]);
            }
        } else {
            if($project->getOriginal('finished')) {
                $project->activities()->create([
                    'user_id' => $user_id,
                    'project_id' => $project->id,
                    'description' => 'unfinish_project'
                ]);
            }
            else {
                $project->activities()->create([
                    'user_id' => $user_id,
                    'project_id' => $project->id,
                    'description' => 'update_project'
                ]);
            }
        }
    }
}
