<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckActive
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (auth()->check() && !auth()->user()->active) {
            auth()->logout();

            $message = 'Twoje konto zostało zablokowane przez administratora.';

            return redirect()->route('login')->withMessage($message);
        }

        return $next($request);
    }
}
