<?php

namespace App\Http\Controllers;

use App\Events\DeletedMessage;
use App\Events\NewMessage;
use App\Events\UpdatedMessage;
use App\Models\Conversation;
use Illuminate\Http\Request;

class ConversationController extends Controller
{
    public function store()
    {
        $conversation = Conversation::create([
            'message' => request('message'),
            'project_id' => request('project_id'),
            'user_id' => auth()->user()->id,
        ]);

        broadcast(new NewMessage($conversation))->toOthers();

        return $conversation->load('user');
    }

    public function update(Request $request, Conversation $conversation)
    {
        $conversation = Conversation::find($conversation->id);
        $conversation->update($request->all());

        broadcast(new UpdatedMessage($conversation))->toOthers();

        return $conversation;
    }

    public function destroy(Conversation $conversation)
    {
        //$this->authorize('delete', $conversation);
        $conversation->delete();

        broadcast(new DeletedMessage($conversation))->toOthers();
    }
}
