<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Models\ProjectRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;

class ProjectTeamController extends Controller
{
    public function users(Project $project)
    {
        $this->authorize('view', $project);
        return view('projects.teams.users', compact('project'));
    }

    public function requests(Project $project, ProjectRequest $request)
    {
        request()->fullUrlWithQuery(['token ' => null]);
        $this->authorize('view', $project);
        $requestsPerPage = 8;
        if($request->id !== null) {

            $count = $project->request->sortByDesc('created_at')->pluck('pivot.id')->search($request->id);

            $page = ((int) ($count / $requestsPerPage) + 1);
            Paginator::currentPageResolver(function () use ($page) {
                return $page;
            });
        }

        $requests = $project->request->sortByDesc('created_at')
            ->paginate($requestsPerPage)
            ->setPath(route('requests', compact('project')));
        return view('projects.teams.requests', compact('project', 'requests'));
    }

    public function destroy(Project $project, User $user)
    {
        $this->authorize('manage', $project);
        $project->team()->detach($user);
//        $project->fresh()->touch();
        return redirect($project->path() . '/team/users');
    }

    public function show(Project $project, User $user)
    {
        if($user->id === $project->user_id) {
            abort(403);
        }

        $this->authorize('manage', $project);
        return view('projects.teams.role', compact('project', 'user'));
    }

    public function update(Project $project, User $user)
    {
        if($user->id === $project->user_id) {
            abort(403);
        }

        $this->authorize('manage', $project);

        \request()->validate(['role' => 'required']);

        $project->getTeamMember($user)->pivot->role = \request('role');
        $project->getTeamMember($user)->pivot->save();

        return redirect()->back();
    }
}
