<?php

namespace App\Http\Controllers;

use App\Http\Requests\InvitationRequest;
use App\Models\Project;
use App\Models\ProjectRequest;
use App\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use function request;

class InvitationController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param Project $project
     * @param InvitationRequest $request
     * @return Application|RedirectResponse|Response|Redirector
     */
    public function store(Project $project, InvitationRequest $request)
    {
        $this->authorize($project, 'invite');
        $user = User::whereEmail(request('email'))->first();
        $project->invite($user);
//        $project->fresh()->touch();
        $request = ProjectRequest::where([['user_id', $user->id], ['project_id', $project->id]])->first();
        if($request!==null) {
            $request->activities->first()->delete();
            $request->delete();
            return redirect($project->path()."/team/requests");
        }
        return redirect($project->path()."/team/users");
    }
}
