<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Storage;
use function request;

class UserProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View|Response
     */
    public function index()
    {
        return view('profiles.index');
    }

    /**
     * Display the specified resource.
     *
     * @param User $user
     * @return Application|Factory|View|Response
     */
    public function show(User $user)
    {
        return view('profiles.show', compact('user'));
    }
    /**
     * Display the specified resource.
     *
     * @param User $user
     * @return Application|Factory|View|Response
     */
    public function activities(User $user)
    {
        $this->authorize('update', $user);

        return view('profiles.activities', compact('user'));
    }


    public function manage(User $user)
    {
        $this->authorize('delete', $user);

        return view('profiles.manage', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param User $user
     * @return Application|Factory|View|Response
     */
    public function edit(User $user)
    {
        $this->authorize('update', $user);

        return view('profiles.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param User $user
     * @return Application|RedirectResponse|Response|Redirector
     */
    public function update(User $user)
    {
        $this->authorize('update', $user);

        $messages = [
            'name.unique' => 'Istnieje użytkownik o podanej nazwie!',
            'name.min' => 'Nazwa użytkownika musi mieć co najmniej 4 znaki!',
            'name.max' => 'Nazwa użytkownika może mieć maksymalnie 30 znaków!',
            'name.required' => 'Nazwa jest wymagana!',
            'email.required' => 'Adres E-Mail jest wymagany!',
            'email.min' => 'Adres E-Mail musi mieć co najmniej 4 znaki!',
            'email.max' => 'Adres E-Mail może mieć maksymalnie 30 znaków!',
            'email.unique' => 'Istnieje użytkownik o podanym adresie E-Mail!',
            'password.min' => 'Hasło musi mieć co najmniej 8 znaków!'
        ];

        if(request('password')) {
            $request = request()->validate([
                'name' => 'required|min:4|max:30|unique:users,name,'.$user->id,
                'email' => 'required|min:4|max:30|unique:users,email,'.$user->id,
                'avatar_image' => 'nullable|image|mimes:jpeg,jpg,png|max:1999',
                'password' => 'nullable|min:8'
            ], $messages );
        } else {
            $request = request()->validate([
                'name' => 'required|min:4|max:30|unique:users,name,'.$user->id,
                'email' => 'required|min:4|max:30|unique:users,email,'.$user->id,
                'avatar_image' => 'nullable|image|mimes:jpeg,jpg,png|max:1999'
            ], $messages);
        }

        if($image = request('avatar_image')) {
            $filenameWithExt = $image->getClientOriginalName();

            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);

            $extension = $image->getClientOriginalExtension();

            $fileNameToStore = $filename . '_' . time() . '.' . $extension;

            $request['avatar_image'] = $fileNameToStore;

            Storage::disk('s3')->put(
                'avatars/' . $fileNameToStore,
                file_get_contents(request('avatar_image')),
                'public'
            );
        }
        $user->update($request);

        return redirect($user->path());
    }

    public function adminUpdate(User $user)
    {
        if(request('role')) {
            $this->authorize('delete', $user);
            $user->role = request('role');
            $user->save();
        }

        if(request('active') !== null) {
            $this->authorize('delete', $user);
            $user->active = request('active');
            $user->save();
        }
        return redirect($user->path()."/manage");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param User $user
     * @return Application|RedirectResponse|Response|Redirector
     */
    public function destroy(User $user)
    {
        $this->authorize('delete', $user);
        $user->delete();
        return redirect('/projects');
    }
}
