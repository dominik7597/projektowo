<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Models\Task;
use App\Models\User;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use function request;

class ProjectTaskController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param Project $project
     * @return Application|RedirectResponse|Response|Redirector
     * @throws AuthorizationException
     */
    public function store(Project $project)
    {
        $this->authorize('createTask', $project);

        request()->validate(['description' => 'required|min:5|max:100']);

        $task = $project->createTask(request('description'));

        if(request('team')) {
            foreach (request('team') as $tm) {
                $tm = User::whereId($tm)->first();
                $task->invite($tm);
                $task->fresh()->touch();
            }
        }

        return redirect($project->path());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Task $task
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function update(Task $task)
    {
        $this->authorize('update', $task);

        request()->validate(['description' => 'required|min:5|max:100']);

        if(request('achieved')) {
            if($task->achieved) {
                abort(403);
            }
            else {
                $task->update([
                    'description' => request('description'),
                    'achieved' => date('Y-m-d H:i:s')
                ]);
            }
        } else {
            $task->update([
                'description' => request('description'),
                'achieved' => null
            ]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Task $task
     * @return RedirectResponse
     * @throws Exception
     */
    public function destroy(Task $task)
    {
        $this->authorize('delete', $task);
        $task->delete();

        return redirect()->back();
    }
}
