<?php

namespace App\Http\Controllers;

use App\Http\Requests\InvitationRequest;
use App\Models\Project;
use App\Models\User;
use Illuminate\Http\Request;

class ProjectRequestController extends Controller
{
    public function show(Project $project, User $user)
    {
        return ($project->request()->find($user->id) ? true : false);
    }

    public function store(Project $project, User $user)
    {
        if ($project->request()->find($user->id) === null) {
            $project->request()->attach($user);
        }
        return redirect()->back();
    }

    public function destroy(Project $project, User $user)
    {
        $project->request()->detach($user);
        return redirect()->back();
    }
}
