<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Project;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Project[]|Collection|Response
     */
    public function index()
    {
        return Project::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        return Project::create($request->all());

    }

    /**
     * Display the specified resource.
     *
     * @param Project $project
     * @return Response
     */
    public function show(Project $project)
    {
        return Project::find($project->id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Project $project
     * @return Response
     */
    public function update(Request $request, Project $project)
    {
        return Project::find($project->id)->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Project $project
     * @return Response
     */
    public function destroy(Project $project)
    {
        return Project::find($project->id)->delete();
    }

    public function search($title)
    {
        return Project::where('title', 'like', '%'.$title.'%')->get();
    }

    public function getTeam(Project $project, Request $request) : JsonResponse
    {
        $limit = 5;

        $query = Project::find($project->id)->team();

        $page = $request->get('page', 1);

        $search = $request->get('search', '');

        if (strlen($search) > 0) {
            $query->where('email', 'like', '%' . $search . '%');
        }

        $query
            ->orderBy('email')
            ->limit($limit + 1)
            ->offset(($page - 1) * $limit);

        $users = $query
            ->pluck('email', 'users.id')
            ->toArray();

        $more = count($users) > $limit;

        if ($more) {
            array_pop($users);
        }

        foreach ($users as $id => &$email) {
            $email = [
                'id' => $id,
                'text' => $email,
            ];
        }

        return response()->json([
            'results' => array_values($users),
            'pagination' => [
                'more' => $more,
            ],
        ]);
    }
}
