<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Activity;
use App\Models\Project;
use App\Models\ProjectRequest;
use App\Models\User;
use Illuminate\Http\Request;

class ProjectRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return ProjectRequest[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Http\Response
     */
    public function index()
    {
        return ProjectRequest::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Project $project
     * @param User $user
     * @return \Illuminate\Http\Response
     */
    public function store(Project $project, User $user)
    {
        if ($project->request()->find($user->id) === null) {
            $project->request()->attach($user);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param Project $project
     * @param User $user
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project, User $user)
    {
        return ProjectRequest::where([['project_id', $project->id], ['user_id', $user->id]])->get();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Project $project
     * @param User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project, User $user)
    {
        $request = ProjectRequest::where([['project_id', $project->id], ['user_id', $user->id]])->first();
        Activity::where('subject_id', $request->fresh()->id)->delete();
        return $request->delete();
    }
}
