<?php

namespace App\Http\Controllers;

use App\Events\UserJoin;
use App\Models\Project;

class ProjectController extends Controller
{
    public function index()
    {
        $activeProjects = auth()->user()->activeProjects()->sortByDesc('updated_at')->paginate(6);

        return view('projects.index', compact('activeProjects'));
    }

    public function unactive()
    {
        $finishedProjects = auth()->user()->finishedProjects()->sortByDesc('updated_at');

        return view('projects.unactive', compact( 'finishedProjects'));
    }

    public function search()
    {
        $this->authorize('viewAny', Project::class);

        return view('projects.search');
    }

    public function activities(Project $project)
    {
        $this->authorize('view', $project);

        return view('projects.show.activities.show', ['project' => $project]);
    }

    public function chat(Project $project)
    {
        $this->authorize('view', $project);

        return view('projects.show.chat.show', ['project' => $project]);
    }

    public function store()
    {
        $this->authorize('create', Project::class);

        $attributes = \request()->validate(['title' => 'required|min:5|max:100',
            'short_body' =>'required|min:5|max:255',
            'body' => 'required|min:5|max:255',
            'is_public' => 'boolean']);

        $user = auth()->user();
        $project = $user->projects()->create($attributes);

        return redirect($project->path());
    }

    public function show(Project $project)
    {
        $this->authorize('view', $project);

        return view('projects.show', compact('project'));
    }

    public function create()
    {
        $this->authorize('create', Project::class);

        return view('projects.create');
    }

    public function update(Project $project)
    {
        if(request('finished') !== $project->getOriginal('finished')) {
            $this->authorize('manage', $project);
        } else {
            $this->authorize('update', $project);
        }

        $rules = ['title' => 'sometimes|required|min:5|max:100',
            'short_body' => 'sometimes|required|min:5|max:255',
            'body' => 'sometimes|required|min:5|max:255',
            'is_public' => 'boolean'];

        $request = \request()->validate($rules);

        if(request('finished')) {
            if($project->finished) {
                $project->update($request);
            }
            else {
                $project->update(['finished' => date('Y-m-d H:i:s')]);
                return redirect('/projects');
            }
        } else {
            $project->update(['finished' => null]);
        }

        $project->update($request);

        return redirect($project->path());
    }

    public function edit(Project $project)
    {
        $this->authorize('update', $project);

        return view('projects.edit', compact('project'));
    }

    public function destroy(Project $project)
    {
        $this->authorize('delete', $project);
        $project->delete();

        return redirect('/projects');
    }
}
