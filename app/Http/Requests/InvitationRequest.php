<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;


class InvitationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Gate::allows('invite', $this->route('project'));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            \request()->validate([
                'email' => [
                    'required',
                    'email',
                    'exists:users,email',
                    function ($attribute, $value, $fail) {

                        $project = $this->route('project');
                        $user = DB::table('users')->where('email', $value);

                        if($project->user->email === $value) {
                            $fail('To twój projekt :)');
                        }

                        if($user->exists()) {
                            $result = DB::table('project_team')->where([
                                ['project_id', $project->id],
                                ['user_id', $user->first()->id]
                            ])->exists();

                            if ($result) {
                                $fail('Ten użytkownik jest już członkiem projektu.');
                            }
                        }
                    }]
            ])
        ];
    }
}
