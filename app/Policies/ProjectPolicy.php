<?php

namespace App\Policies;

use App\Models\Project;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProjectPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param User $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->isActive();
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param User $user
     * @param Project $project
     * @return mixed
     */
    public function view(User $user, Project $project)
    {
        return $user->is($project->user) || $project->team->contains($user);
    }

    /**
     * Determine whether the user can create models.
     *
     * @param User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->isActive();
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param User $user
     * @param Project $project
     * @return mixed
     */
    public function update(User $user, Project $project)
    {
        if($project->finished) {
            return false;
        }

        $role = $project->getTeamMemberRole($user);
        if($role === 'update' || $role === 'manage') {
            return true;
        }

        return $user->is($project->user);
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param User $user
     * @param Project $project
     * @return mixed
     */
    public function manage(User $user, Project $project)
    {
        $role = $project->getTeamMemberRole($user);
        if($role === 'manage') {
            return true;
        }

        return $user->is($project->user);
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param User $user
     * @param Project $project
     * @return mixed
     */
    public function invite(User $user, Project $project)
    {
        if($project->finished) {
            return false;
        }

        $role = $project->getTeamMemberRole($user);
        if($role === 'update' || $role === 'manage') {
            return true;
        }

        return $user->is($project->user);
    }

    public function createTask(User $user, Project $project)
    {
        if($project->finished) {
            return false;
        }

        $role = $project->getTeamMemberRole($user);
        if($role === 'update' || $role === 'manage') {
            return true;
        }

        return $user->is($project->user);
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param User $user
     * @param Project $project
     * @return mixed
     */
    public function delete(User $user, Project $project)
    {
        if(!$project->finished) {
            return false;
        }

        $role = $project->getTeamMemberRole($user);
        if($role === 'manage') {
            return true;
        }

        return $user->is($project->user);
    }
}
