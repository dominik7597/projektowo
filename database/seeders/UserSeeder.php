<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::factory([
            'name' => 'admin',
            'email' => 'admin@test',
            'password' => Hash::make('testtest'),
            'role' => 'admin'
        ])->create();

        User::factory([
            'name' => 'test',
            'email' => 'test@test',
            'password' => Hash::make('testtest'),
        ])->create();

        User::factory([
            'name' => 'test2',
            'email' => 'test2@test',
            'password' => Hash::make('testtest'),
        ])->create();

        User::factory()
            ->count(5)
            ->create();

        User::factory([
            'name' => 'jan.kowalski',
            'email' => 'jan.k@test',
            'password' => Hash::make('testtest'),
            'avatar_image' => 'avatar1.jpg',
        ])->create();

        User::factory([
            'name' => 'jan.nowak',
            'email' => 'jan.n@test',
            'password' => Hash::make('testtest'),
            'avatar_image' => 'avatar2.jpg',
        ])->create();

        User::factory([
            'name' => 'adam.polak',
            'email' => 'adam.p@test',
            'password' => Hash::make('testtest'),
            'avatar_image' => 'avatar3.jpg',
        ])->create();

        User::factory([
            'name' => 'alicja.wiśniewska',
            'email' => 'alicja.p@test',
            'password' => Hash::make('testtest'),
            'avatar_image' => 'avatar4.jpg',
        ])->create();
    }
}
