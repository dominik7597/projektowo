<?php

namespace Database\Seeders;

use App\Models\Memo;
use App\Models\Project;
use App\Models\Task;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class ProjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 0; $i < 10; $i++) {
            $project = Project::factory()
                ->has(Task::factory()->count(3))
                ->has(Memo::factory()->count(5))
                ->for(User::where('name', 'test')->first())
                ->create();

            $userToInvite = User::where('name', 'test2')->first();

            $project->invite($userToInvite);

            for($j = 0; $j < 10; $j++) {
                $user = User::factory()->create();
                $project->invite($user);
            }

            $project->tasks->each(function ($task) use ($project){
                $task->team()->attach($project->fresh()->team->random(rand(1,5)));
            });

            for($j = 0; $j < 10; $j++) {
                $user = User::factory()->create();
                $project->request()->attach($user);
            }
        }

        for($i = 0; $i < 3; $i++) {
            $project = Project::factory(['finished' => now()])
                ->has(Task::factory()->count(3))
                ->for(User::where('name', 'test')->first())
                ->create();

            $userToInvite = User::where('name', 'test2')->first();

            $project->invite($userToInvite);

            for($j = 0; $j < 10; $j++) {
                $user = User::factory()->create();
                $project->invite($user);
            }
        }
    }
}
