<?php

namespace Database\Seeders;

use App\Models\Project;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::disableSearchSyncing();
        Project::disableSearchSyncing();

        $this->call([
            UserSeeder::class,
            ProjectSeeder::class,
            BigProject::class
        ]);

        User::all()->searchable();
        Project::all()->searchable();

        User::enableSearchSyncing();
        Project::enableSearchSyncing();
    }
}
