<?php

namespace Database\Seeders;

use App\Models\Memo;
use App\Models\Project;
use App\Models\Task;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class BigProject extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $time = now()->addMinutes(-30);
        Carbon::setTestNow($time);

        $project = Project::factory([
            'title' => 'Sklep internetowy',
            'short_body' => 'Projekt mający na celu stworzenie sklepu internetowego. Implementacja aplikacji opiera się na wykorzystaniu frameworka Laravel oraz przy użyciu środowiska programistycznego PHPStorm.',
            'body' => '<h2>Etap 1</h2>Zaczynam od zaproszenia użytkowników i przydzielenia im uprawnień. Razem rodzielimy zadania do realizacji.',
            'is_public' => 0,
        ])
            ->for($user = User::where('name', 'jan.kowalski')->first())
            ->create();

        Carbon::setTestNow($time->addMinutes(1));

        $task = $project->createTask('Zaprosić wszystkich uczestników i nadać im uprawnienia.');

        $project->createMemo('Jan, Adam - pełne prawa, Alicja - tylko edycja!');

        Carbon::setTestNow($time->addMinutes(1));

        $usersToInvite = [
            $jan = User::where('name', 'jan.nowak')->first(),
            $adam = User::where('name', 'adam.polak')->first(),
            $alicja = User::where('name', 'alicja.wiśniewska')->first()
        ];

        Carbon::setTestNow($time->addMinutes(1));

        foreach ($usersToInvite as $user) {
            $project->invite($user);
            $project=$project->fresh();
            $project->getTeamMember($user)->pivot->role='update';
            $project->getTeamMember($user)->pivot->save();
        }

        $project->createConversation('Witam wszystkich w zespole :)', $project->user->id);

        Carbon::setTestNow($time->addMinutes(1));

        $task->achieved=now();
        $task->save();

        Carbon::setTestNow($time->addMinutes(1));

        $task1 = $project->createTask('Rozdzielić zadania.');

        Carbon::setTestNow($time->addMinutes(1));

        $task2 = $project->createTask('Stworzyć diagram przypadków użycia.');
        $task2->team()->attach($jan);

        $project->createMemo('https://pl.wikipedia.org/wiki/Diagram_przypadków_użycia');

        Carbon::setTestNow($time->addMinutes(1));

        $task3 = $project->createTask('Zaprojektować bazę danych.');
        $task3->team()->attach($adam);
        $task3->team()->attach($alicja);

        Carbon::setTestNow($time->addMinutes(1));

        $task4 = $project->createTask('Rozpocząć implementację (przygotowanie środowiska).');
        $task4->team()->attach($project->user_id);

        Carbon::setTestNow($time->addMinutes(1));

        $task1->achieved=now();
        $task1->save();
    }
}
